﻿using System;

namespace ExtensionMethods
{
    internal class Complex : IComplex
    {
        private readonly double re;
        private readonly double im;

        public Complex(double re, double im)
        {
            this.re = re;
            this.im = im;
        }

        public bool Equals(IComplex other)
        {
            return this.Real == other.Real && this.Imaginary == other.Imaginary;
        }

        public double Real => re;

        public double Imaginary => im;

        public double Modulus => Math.Sqrt(Real * Real + Imaginary * Imaginary);
       
        public double Phase =>  Math.Atan2(Imaginary, Real);
        
        public override string ToString()
        {
            return Real.ToString() + Imaginary.ToString();
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Real.GetHashCode() * Imaginary.GetHashCode();
        }

    }
}