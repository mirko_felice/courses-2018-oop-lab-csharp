using System;
using System.Collections.Generic;

namespace Iterators {

    public static class Java8StreamOperations
    {
        public static void ForEach<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            foreach (var v in sequence)
            {
                consumer.Invoke(v);
            }
        }

        public static IEnumerable<TAny> Peek<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            foreach (var v in sequence)
            {
                consumer.Invoke(v);
                yield return v;
            }
        }

        public static IEnumerable<TOther> Map<TAny, TOther>(this IEnumerable<TAny> sequence, Func<TAny, TOther> mapper)
        {
            foreach (var v in sequence)
            {
                yield return mapper.Invoke(v);
            }
        }

        public static IEnumerable<TAny> Filter<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            foreach (var v in sequence)
            {
                if (consumer.Invoke(v))
                {
                    yield return v;
                }
            }
        }

        public static IEnumerable<Tuple<int, TAny>> Indexed<TAny>(this IEnumerable<TAny> sequence)
        {
            int i = 0;
            foreach (var v in sequence)
            {
                yield return new Tuple<int, TAny>(i,v);
                i++;
            }
        }

        public static TOther Reduce<TAny, TOther>(this IEnumerable<TAny> sequence, TOther seed, Func<TOther, TAny, TOther> reducer)
        {
            foreach (var v in sequence)
            {
                reducer.Invoke(seed, v);
            }
            return seed;
        }

        public static IEnumerable<TAny> SkipWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            foreach (var v in sequence)
            {
                while (consumer.Invoke(v)) { continue; }
                yield return v;
            }
        }

        public static IEnumerable<TAny> SkipSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            var i = sequence.GetEnumerator();
            int ind = 0;
            TAny a;
            while (i.MoveNext() && ind < count)
            {
                a = i.Current;
            }
            while (i.MoveNext())
            {
                yield return i.Current;
            }
        }

        public static IEnumerable<TAny> TakeWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<TAny> TakeSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<int> Integers(int start)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<int> Integers()
        {
            return Integers(0);
        }

        public static IEnumerable<int> Range(int start, int count)
        {
            return Integers().TakeSome(count);
        }
    }

}