﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        private Dictionary<Tuple<TKey1, TKey2>, TValue> Map = new Dictionary<Tuple<TKey1, TKey2>, TValue>();

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if (other is Map2D<TKey1, TKey2, TValue>)
            {
                Map2D<TKey1, TKey2, TValue> oth = other as Map2D<TKey1, TKey2, TValue>;
                return oth.Map.Keys.Equals(this.Map.Keys) && oth.Map.Values.Equals(this.Map.Values);
            }
            return false;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get
            {
                if (Map.ContainsKey(new Tuple<TKey1, TKey2>(key1, key2)))
                {
                    Map.TryGetValue(new Tuple<TKey1, TKey2>(key1, key2),out TValue value);
                    return value;
                }
                return default(TValue);
            }
            set
            {
                if (Map.ContainsKey(new Tuple<TKey1, TKey2>(key1, key2)))
                {
                    Map.TryGetValue(new Tuple<TKey1, TKey2>(key1, key2), out TValue valueToSet);
                    valueToSet = value;
                }
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> list = new List<Tuple<TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> tuple in Map.Keys)
            {
                if (tuple.Item1.Equals(key1))
                {
                    list.Add(new Tuple<TKey2, TValue>(tuple.Item2, this[key1, tuple.Item2]));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> list = new List<Tuple<TKey1, TValue>>();
            foreach (Tuple<TKey1, TKey2> tuple in Map.Keys)
            {
                if (tuple.Item2.Equals(key2))
                {
                    list.Add(new Tuple<TKey1, TValue>(tuple.Item1, this[tuple.Item1, key2]));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> list = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> tuple in Map.Keys)
            {
                Map.TryGetValue(new Tuple<TKey1, TKey2>(tuple.Item1, tuple.Item2), out TValue value);
                list.Add(new Tuple<TKey1, TKey2, TValue>(tuple.Item1, tuple.Item2, value));
            }
            return list;
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            TKey1[] arrayKey1 = keys1.ToArray();
            TKey2[] arrayKey2 = keys2.ToArray();
            IList<Tuple<TKey1, TKey2>> list = new List<Tuple<TKey1, TKey2>>();
            for (int i = 0; i < arrayKey1.Length; i++)
            {
                for(int j = 0; j < arrayKey2.Length; j++)
                {
                    list.Add(new Tuple<TKey1, TKey2>(arrayKey1[i], arrayKey2[j]));
                }
                
            }
            foreach(Tuple<TKey1, TKey2> tuple in list)
            {
                Map.Add(tuple,generator(tuple.Item1, tuple.Item2));
            }
        }

        public int NumberOfElements
        {
            get => Map.Count;
        }

        public override string ToString()
        {
            string outString = "";
            foreach (Tuple<TKey1,TKey2> tuple in Map.Keys)
            {
                outString += " keys : [" + tuple.Item1.ToString() + ", " + tuple.Item2.ToString() + "]";
                Map.TryGetValue(new Tuple<TKey1, TKey2>(tuple.Item1, tuple.Item2), out TValue value);
                outString += " value : " + value.ToString() + "\n";
            }
            return outString;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Map.GetHashCode();
        }
    }
}
